"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;


// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here



//If y, then print true
if (userResponse == 'y' || userResponse == 'Y')  {
   // userResponseAnswer = true;
   console.log("userResponse: " + true);
} else {
   // userResponseAnswer = false;
   console.log("userResponse: " + false);
}

//if firstname starts from letter B
if( firstName.charAt(0).toLowerCase() == 'b') {

    console.log("firstname = " + true);
} else {
    console.log("firstname = " + false);
}
//If singer is equal to taylor swift
if(singer == "Taylor Swift") {
    console.log("Singer = " + true);
} else { 
    console.log("Singer = " + false);
}

//If the yearborn is greater than 1978 but not equal to 2013
if(year > 1978 && year != 2013) {
    console.log("yearBorn = " + true);
} else {
    console.log("yearBorn = " + false);
}
