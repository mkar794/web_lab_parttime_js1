"use strict";

// Provided variables
var string1 = "Hello World";
var string2 = "Hi everybody!"
var string3 = "Hi, Dr Nick!"

// TODO Your answers here.
console.log("string1 = " + string1.length);
console.log("substring of string1 = " + string1.substring(8));
console.log(string2.indexOf(" ") != -1);
console.log(string3.charAt(string3.length-1));
console.log(string3.indexOf("!"));
