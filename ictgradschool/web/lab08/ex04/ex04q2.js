"use strict";

// Provided variables.
var year = 2018;
//var year = 2018;

// Variables you'll be assigning to in this question.
var isLeapYear;

// TODO Your code for part (2) here.

// function leapYear(questionYear) {
//     if(questionYear % 4 == 0) {
//         isLeapYear = true;
//     }
//     else if(!(questionYear % 100 == 0)) {
//         isLeapYear = true;
//     }
//     else if(questionYear % 400 == 0) {
//         isLeapYear = true;
//     } 
//     else {
//         isLeapYear = false;
//     }
//     return isLeapYear;
    
// }


// // Printing the answer
// if (leapYear(year)) {
//     console.log("Part 2: " + year + " is a leap year.");
// } else {
//     console.log("Part 2: " + year + " is NOT a leap year.");
// }



// Printing the answer
if ((0 == year % 4) && (0 != year % 100) || (0 == year % 400)) {
    console.log("Part 2: " + year + " is a leap year.");
} else {
    console.log("Part 2: " + year + " is NOT a leap year.");
}



